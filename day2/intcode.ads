with ADA.Text_IO; use ADA.Text_IO;

package Intcode is
	procedure GetIntAt(Position : IN Integer; result : OUT Integer);
	procedure Interprete(Noun, Verb : IN Integer; Result : OUT Integer);
	procedure UpdateIntCode(ResultPosition, Result : Integer);
end Intcode;

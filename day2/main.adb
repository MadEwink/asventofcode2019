with Intcode; use IntCode;
with ADA.Text_IO; use ADA.Text_IO;

procedure Main is
	Noun, Verb : Integer;
	Result : Integer;
	ExpectedResult : Integer := 19690720;
begin
	FIND_PAIR:
	for Noun in 0..99 loop
		for Verb in 0..99 loop
			Interprete(Noun,Verb,Result);
			exit FIND_PAIR when Result = ExpectedResult;
		end loop;
	end loop FIND_PAIR;
end Main;


package CrossWire is
	type Direction is ('U', 'D', 'L', 'R');
	type WireDirection is
		record
			dir : Direction;
			len : Integer;
		end record;
	type Coordinates is array (1..2) of Integer;
	type Intersection is
		record
			crd : Coordinates := (0,0);
			firstWireSteps : Integer;
			secondWireSteps : Integer;
		end record;
	procedure GetLengths;
	procedure ProgressThrough(Dir : IN Direction; Coordinate : IN OUT Coordinates);
	function GetManathanDistance(Coordinate : Coordinates) return Integer;
	function Equals(C1, C2 : Coordinates) return Boolean;
	procedure ExplorePaths;
end CrossWire;

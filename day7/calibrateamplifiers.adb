with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with IntCode; use IntCode;

procedure CalibrateAmplifiers is
	procedure ComputeResult(Phase : Integer; Result : IN OUT Integer) is
		PhaseFile, ResultFile : File_Type;
	begin
		-- write phase and input into a file
		Create(PhaseFile, OUT_FILE, "phase_input");
		Put(PhaseFile, Phase'Image & Result'Image);
		Close(PhaseFile);
		-- set this file as input
		Open(PhaseFile, IN_FILE, "phase_input");
		Set_Input(PhaseFile);
		-- set this other file as output
		Create(ResultFile, OUT_FILE, "result");
		Set_Output(ResultFile);
		-- launch code
		IntCode.Interprete;
		Close(ResultFile);
		-- read result from file
		Open(ResultFile, IN_FILE, "result");
		Get(ResultFile, Result);
		-- delete files
		Delete(PhaseFile);
		Delete(ResultFile);
	end ComputeResult;

	Phase5 : Integer;
	Result1, Result2, Result3, Result4, Result5 : Integer;
	MaximumOutput : Integer := 0;
	File : File_Type;
begin
	for Phase1 in 0..4 loop
		Result1 := 0;
		ComputeResult(Phase1, Result1);
		for Phase2 in 0..4 loop
			if Phase2 /= Phase1 then
				Result2 := Result1;
				ComputeResult(Phase2, Result2);
				for Phase3 in 0..4 loop
					if Phase3 /= Phase2 and Phase3/= Phase1 then
						Result3 := Result2;
						ComputeResult(Phase3, Result3);
						for Phase4 in 0..4 loop
							if Phase4 /= Phase3 and Phase4 /= Phase2 and Phase4 /= Phase1 then
								Phase5 := 0;
								while Phase5 = Phase1 or Phase5 = Phase2 or Phase5 = Phase3 or Phase5 = Phase4 loop
									Phase5 := Phase5+1;
								end loop;
								Result4 := Result3;
								ComputeResult(Phase4, Result4);
								Result5 := Result4;
								ComputeResult(Phase5, Result5);
								if Result5 > MaximumOutput then
									MaximumOutput := Result5;
								end if;
							end if;
						end loop;
					end if;
				end loop;
			end if;
		end loop;
	end loop;
	Put_Line(Standard_Output, MaximumOutput'Image);
end CalibrateAmplifiers;

generic
Size_X : Positive;
Size_Y : Positive;
package MonitoringStation is
	type AsteroidMap is array (0..Size_X-1, 0..Size_Y-1) of Integer;
	procedure PrintMap(Map : IN AsteroidMap);
	function ReadFromFile(FileName : String) return AsteroidMap;
	function CanSeeEachOther(Map : IN AsteroidMap; X1,Y1 : Natural; X2,Y2 : Natural) return Boolean;
	procedure CalculateSights(Map : IN OUT AsteroidMap);
	procedure FindBestSight(Map : IN AsteroidMap; X,Y : OUT Natural);
	procedure FindDestroyedAsteroid(Map : IN OUT AsteroidMap; X0,Y0 : IN Natural; AsteroidNumber : IN Positive; Xt,Yt : OUT Natural);
end MonitoringStation;

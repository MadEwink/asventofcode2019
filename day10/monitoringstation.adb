with Ada.Text_IO; use Ada.Text_IO;

package body MonitoringStation is
	procedure PrintMap(Map : IN AsteroidMap) is
	begin
		for Y in 0..Size_Y-1 loop
			for X in 0..Size_X-1 loop
				if (Map(X,Y) < 0) then
					Put(".");
				else
					Put("#");
				end if;
			end loop;
			Put_Line(" ");
		end loop;
	end PrintMap;
	
	procedure NextMapIndex(X,Y : IN OUT Natural) is
	begin
		X := X + 1;
		if X >= Size_X then
			X := 0;
			Y := Y + 1;
		end if;
	end NextMapIndex;

	procedure NextBorderItem(X,Y : IN OUT Natural) is
	begin
		-- on the upper border : go right
		if Y = 0 then
			if X = Size_X-1 then
				Y := 1;
			else
				X := X+1;
			end if;
		-- on the right border : go down
		elsif X = Size_X-1 then
			if Y = Size_Y-1 then
				X := X-1;
			else
				Y := Y+1;
			end if;
		-- on the down border : go left
		elsif Y = Size_Y-1 then
			if X = 0 then
				Y := Y-1;
			else
				X := X-1;
			end if;
		-- on the left border : go up
		elsif X = 0 then
			if Y = 0 then
				X := X+1;
			else
				Y := Y-1;
			end if;
		end if;
	end NextBorderItem;

	function ReadFromFile(FileName : String) return AsteroidMap is
		File : File_Type;
		Map : AsteroidMap := (others => (others => -1));
		c : Character;
		IndexX, IndexY : Natural := 0;
	begin
		Open(File, IN_FILE, FileName);
		while not End_Of_File(File) loop
			Get(File,c);
			if c = '.' then
				Map(IndexX, IndexY) := -1;
			else
				Map(IndexX, IndexY) := 0;
			end if;
			NextMapIndex(IndexX, IndexY);
		end loop;
		Close(File);
		return Map;
	end ReadFromFile;

	function CanSeeEachOther(Map : IN AsteroidMap; X1,Y1 : Natural; X2,Y2 : Natural) return Boolean is
		Xmin : Natural := Integer'Min(X1,X2);
		Xmax : Natural := X1+X2-Xmin;
		Ymin : Natural := Integer'Min(Y1,Y2);
		Ymax : Natural := Y1+Y2-Ymin;
		a : Float;
		b : Float;
	begin
		if Xmax-Xmin > 1 then
			-- Y = aX+b
			a := Float(Y2-Y1)/Float(X2-X1);
			b := Float(Y1) - a*Float(X1);
			for X in Xmin+1..Xmax-1 loop
				declare
					Y : Float := a*Float(X)+b;
				begin
					if Y = Float'Truncation(Y) then
						if Map(X,Natural(Y)) >= 0 then
							return False;
						end if;
					end if;
				end;
			end loop;
		end if;
		if Ymax-Ymin > 1 then
			-- X = aY+b
			a := Float(X2-X1)/Float(Y2-Y1);
			b := Float(X1) - a*Float(Y1);
			for Y in Ymin+1..Ymax-1 loop
				declare
					X : Float := a*Float(Y)+b;
				begin
					if X = Float'Truncation(X) then
						if Map(Natural(X),Y) >= 0 then
							return False;
						end if;
					end if;
				end;
			end loop;
		end if;
		return True;
	end CanSeeEachOther;
	procedure CalculateSights(Map : IN OUT AsteroidMap) is
		X2, Y2 : Natural;
	begin
		for Y1 in 0..Size_Y-1 loop
			for X1 in 0..Size_X-1 loop
				-- for each element, examine the distance to all the following elements
				if Map(X1,Y1) >= 0 then
					X2 := X1;
					Y2 := Y1;
					loop
						NextMapIndex(X2,Y2);
						exit when Y2 >= Size_Y;
						if Map(X2,Y2) >= 0 and CanSeeEachOther(Map, X1, Y1, X2, Y2) then
							Map(X1,Y1) := Map(X1,Y1)+1;
							Map(X2,Y2) := Map(X2,Y2)+1;
						end if;
					end loop;
				end if;
			end loop;
		end loop;
	end CalculateSights;

	procedure FindBestSight(Map : IN AsteroidMap; X,Y : OUT Natural) is
		BestSight : Integer := Map(0,0);
	begin
		for CurrentY in 0..Size_Y-1 loop
			for CurrentX in 0..Size_X-1 loop
				if BestSight < Map(CurrentX,CurrentY) then
					BestSight := Map(CurrentX,CurrentY);
					X := CurrentX;
					Y := CurrentY;
				end if;
			end loop;
		end loop;
	end FindBestSight;

	function DestroyAsteroidOnLine(Map : IN OUT AsteroidMap; X0,Y0 : IN Natural; Xb,Yb : IN Natural; Xt, Yt : OUT Natural) return Boolean is
		a,b : Float;
		Xmin : Natural := Integer'Min(X0,Xb);
		Xmax : Natural := X0+Xb-Xmin;
		Ymin : Natural := Integer'Min(Y0,Yb);
		Ymax : Natural := Y0+Yb-Ymin;
	begin
		Xt := X0;
		Yt := Y0;
		if Xmax-Xmin > 1 then
			-- Y = aX+b
			a := Float(Yb-Y0)/Float(Xb-X0);
			b := Float(Y0) - a*Float(X0);
			for X in Xmin+1..Xmax-1 loop
				declare
					Y : Float := a*Float(X)+b;
				begin
					if Y = Float'Truncation(Y) then
						if Map(X,Natural(Y)) >= 0 then
							-- destroy asteroid
							Xt := X;
							Yt := Natural(Y);
							Map(Xt,Yt) := -1;
							return True;
						end if;
					end if;
				end;
			end loop;
		end if;
		if Ymax-Ymin > 1 then
			-- X = aY+b
			a := Float(Xb-X0)/Float(Yb-Y0);
			b := Float(X0) - a*Float(Y0);
			for Y in Ymin+1..Ymax-1 loop
				declare
					X : Float := a*Float(Y)+b;
				begin
					if X = Float'Truncation(X) then
						if Map(Natural(X),Y) >= 0 then
							Xt := Natural(X);
							Yt := Y;
							Map(Xt,Yt) := -1;
							return True;
						end if;
					end if;
				end;
			end loop;
		end if;
		-- if not exited, the location is visible
		if Map(Xb,Yb) >= 0 then
			Xt := Xb;
			Yt := Yb;
			Map(Xt,Yt) := -1;
			return True;
		end if;
		-- we did not destroy nothing
		return False;
	end DestroyAsteroidOnLine;

	procedure FindDestroyedAsteroid(Map : IN OUT AsteroidMap; X0,Y0 : IN Natural; AsteroidNumber : IN Positive; Xt,Yt : OUT Natural) is
		NumberDestroyed : Natural := 0;
		-- start on the upper border, just above the station
		Xb : Natural := X0;
		Yb : Natural := 0;
	begin
		loop
			if DestroyAsteroidOnLine(Map, X0,Y0, Xb,Yb, Xt,Yt) then
				NumberDestroyed := NumberDestroyed+1;
			end if;
			Put_Line(NumberDestroyed'Image);
			PrintMap(Map);
			exit when NumberDestroyed = AsteroidNumber;
			NextBorderItem(Xb,Yb);
			--Put_Line(Xb'Image & Yb'Image);
		end loop;
	end FindDestroyedAsteroid;
end MonitoringStation;

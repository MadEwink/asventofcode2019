with Orbit;

procedure Main is
begin
	Orbit.ReadInput;
	Orbit.ComputeOrbits;
	Orbit.DistanceToSanta;
end Main;

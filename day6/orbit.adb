with Ada.Text_IO;
use Ada.Text_IO;

package body Orbit is
	use OrbitTree;
	OrbitsTree : Tree;
	
	procedure AppendChildsFromFile(Position : IN Cursor) is
		File : FILE_TYPE;
		OrbitCenter : Star;
		OrbitingObject : Star;
		Star1String : String(1..3);
		Star2String : String(1..3);
		RelationChar : Character;
	begin
		Open(File, IN_FILE, "input");
		OrbitCenter := Element(Position);
		while not End_Of_File(File) loop
			Get(File, Star1String);
			Get(File, RelationChar);
			Get(File, Star2String);
			if Star1String = OrbitCenter.name then
				OrbitingObject.name := Star2String;
				Append_Child(OrbitsTree, Position, OrbitingObject);
			end if;
		end loop;
		Close(File);
	end AppendChildsFromFile;
	
	procedure RecursiveAppend(Position : IN Cursor) is
		LocalPosition : Cursor;
	begin
		AppendChildsFromFile(Position);
		if Child_Count(Position) > 0 then
			LocalPosition := First_Child(Position);
			while LocalPosition /= No_Element loop
				RecursiveAppend(LocalPosition);
				Next_Sibling(LocalPosition);
			end loop;
		end if;
	end RecursiveAppend;

	procedure ReadInput is
		Com : Star := (Name => "COM");
	begin
		Append_Child(OrbitsTree, Root(OrbitsTree), Com);
		RecursiveAppend(First_Child(Root(OrbitsTree)));
	end ReadInput;

	procedure RecursiveCount(Position : IN Cursor; Count: IN OUT Count_Type) is
		LocalPosition : Cursor;
	begin
		Count := Count + Depth(Position) - 2;
		if Child_Count(Position) > 0 then
			LocalPosition := First_Child(Position);
			while LocalPosition /= No_Element loop
				RecursiveCount(LocalPosition, Count);
				Next_Sibling(LocalPosition);
			end loop;
		end if;
	end RecursiveCount;

	procedure ComputeOrbits is
		OrbitsCount : Count_Type := 0;
	begin
		RecursiveCount(First_Child(Root(OrbitsTree)), OrbitsCount);
		Put_Line(OrbitsCount'Image);
	end ComputeOrbits;

	function FirstCommonStar(Pos1,Pos2 : Cursor) return Cursor is
		LPos1 : Cursor := Pos1;
		LPos2 : Cursor := Pos2;
	begin
		while LPos1 /= LPos2 loop
			if (Depth(LPos1) > Depth(LPos2)) then
				LPos1 := Parent(LPos1);
			else
				LPos2 := Parent(LPos2);
			end if;
		end loop;
		return LPos1;
	end FirstCommonStar;

	function GetDistance(Pos1, Pos2 : Cursor) return Count_Type is
		CommonStar : Cursor := FirstCommonStar(Pos1,Pos2);
	begin
		-- the 2 corresponds to distance YOU-DirectOrbit, SAN-DirectOrbit
		return (Depth(Pos1) - Depth(CommonStar)) + (Depth(Pos2) - Depth(CommonStar)) - 2;
	end GetDistance;

	procedure DistanceToSanta is
		YOU : Star := (name=>"YOU");
		SAN : Star := (name=>"SAN");
		PosYou, PosSan : Cursor;
	begin
		PosYou := Find(OrbitsTree, YOU);
		PosSan := Find(OrbitsTree, SAN);
		Put_Line(GetDistance(PosYou,PosSan)'Image);
	end DistanceToSanta;

end Orbit;

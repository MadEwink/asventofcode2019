with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

package body Intcode is
	IntCodeFileName : String := "input";

	function GetCodeLength return Integer is
		File : File_Type;
		Result : Integer := 0;
		Element : Integer;
		c : Character;
	begin
		Open(File, IN_FILE, IntCodeFileName);
		loop
			Get(File, Element); -- get the intcode element
			Result := Result + 1;
			exit when (End_Of_File(File));
			Get(File, c); -- get the comma
		end loop;
		Close(File);
		return Result;
	end GetCodeLength;

	function GetIntCode(CodeLength : Integer) return Vector is
		IntCodeVector : Vector(0..CodeLength-1) := (others => 0);
		File : File_Type;
		Element : Integer;
		c : Character;
	begin
		Open(File, IN_FILE, IntCodeFileName);
		for Index in IntCodeVector'Range loop
			Get(File, Element);
			IntCodeVector(Index) := Element;
			exit when End_Of_File(File);
			Get(File,c);
		end loop;
		Close(File);
		return IntCodeVector;
	end GetIntCode;

	procedure ExecuteInstruction(IntCodeVector : IN OUT Vector; InstructionPosition : IN OUT Integer; Instruction : IN Integer; Value1, Value2, Address : IN Integer) is
		Element : Integer;
	begin
		case Instruction is
			when 1 =>
				IntCodeVector(Address) := Value1+value2;
				InstructionPosition := InstructionPosition + 4;
			when 2 =>
				IntCodeVector(Address) := Value1*value2;
				InstructionPosition := InstructionPosition + 4;
			when 3 =>
				Put("Enter input : ");
				Get(Standard_Input, Element);
				IntCodeVector(Address) := Element;
				InstructionPosition := InstructionPosition + 2;
			when 4 =>
				Put_Line(IntCodeVector(Address)'Image);
				InstructionPosition := InstructionPosition + 2;
			when 5 =>
				if Value1 /= 0 then
					InstructionPosition := Address;
				else
					InstructionPosition := InstructionPosition + 3;
				end if;
			when 6 =>
				if Value1 = 0 then
					InstructionPosition := Address;
				else
					InstructionPosition := InstructionPosition + 3;
				end if;
			when 7 =>
				if Value1 < Value2 then
					IntCodeVector(Address) := 1;
				else
					IntCodeVector(Address) := 0;
				end if;
				InstructionPosition := InstructionPosition + 4;
			when 8 =>
				if Value1 = Value2 then
					IntCodeVector(Address) := 1;
				else
					IntCodeVector(Address) := 0;
				end if;
				InstructionPosition := InstructionPosition + 4;
			when others => null;
		end case;
	end ExecuteInstruction;

	procedure PositionInstruction(IntCodeVector : IN Vector; InstructionPosition : IN Integer; Instruction, Value1, Value2, Address : OUT Integer) is
		Modes : array (Integer range 1..3) of Integer := (others => 0);
	begin
		Instruction := IntCodeVector(InstructionPosition);
		if Instruction > 99 then
			Modes(1) := (Instruction/100) - 10*(Instruction/1000);
			Modes(2) := (Instruction/1000) - 10*(Instruction/10000);
			Modes(3) := (Instruction/10000) - 10*(Instruction/100000);
		end if;
		Instruction := Instruction - 100*(Instruction/100);
		case Instruction is
			when 1 | 2 =>
				if Modes(1) = 0 then
					Value1 := IntCodeVector(IntCodeVector(InstructionPosition+1));
				else
					Value1 := IntCodeVector(InstructionPosition+1);
				end if;
				if Modes(2) = 0 then
					Value2 := IntCodeVector(IntCodeVector(InstructionPosition+2));
				else
					Value2 := IntCodeVector(InstructionPosition+2);
				end if;
				Address := IntCodeVector(InstructionPosition+3);
			when 3 | 4 =>
				Value1 := 0;
				Value2 := 0;
				Address := IntCodeVector(InstructionPosition+1);
			when 5 | 6 =>
				if Modes(1) = 0 then
					Value1 := IntCodeVector(IntCodeVector(InstructionPosition+1));
				else
					Value1 := IntCodeVector(InstructionPosition+1);
				end if;
				Value2 := 0;
				if Modes(2) = 0 then
					Address := IntCodeVector(IntCodeVector(InstructionPosition+2));
				else
					Address := IntCodeVector(InstructionPosition+2);
				end if;
			when 7 | 8 =>
				if Modes(1) = 0 then
					Value1 := IntCodeVector(IntCodeVector(InstructionPosition+1));
				else
					Value1 := IntCodeVector(InstructionPosition+1);
				end if;
				if Modes(2) = 0 then
					Value2 := IntCodeVector(IntCodeVector(InstructionPosition+2));
				else
					Value2 := IntCodeVector(InstructionPosition+2);
				end if;
				Address := IntCodeVector(InstructionPosition+3);
			when others =>
				Value1 := 0;
				Value2 := 0;
				Address := 0;
		end case;
	end PositionInstruction;

	procedure Interprete is
		CurrentInstructionPosition : Integer := 0;
		CurrentInstruction : Integer;
		Value1,Value2,Address : Integer;
		IntCodeVector : Vector := GetIntCode(GetCodeLength);
	begin
		loop
			PositionInstruction(IntCodeVector, CurrentInstructionPosition, CurrentInstruction, Value1, Value2, Address);
			--Put_Line(CurrentInstructionPosition'Image & ' ' & CurrentInstruction'Image & ' ' & Value1'Image & ' ' & Value2'Image & ' ' & Address'Image);
			exit when CurrentInstruction = 99;
			ExecuteInstruction(IntCodeVector, CurrentInstructionPosition, CurrentInstruction, Value1, Value2, Address);
		end loop;
	end Interprete;
end Intcode;


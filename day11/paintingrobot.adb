with Intcode; use Intcode;

procedure PaintingRobot is
	type BoardPosition;
	type BoardPositionPtr is access BoardPosition;
	type BoardPosition is
		record
			X : Integer;
			Y : Integer;
			State : Boolean;
			Previous : BoardPositionPtr;
		end record;
	type Direction is (Up, Right, Down, Left);
	-- Rotates Current clockwise (Instruction true) or counter-clockwise (Instruction false)
	procedure UpdateDirection(Current : IN OUT Direction ; Instruction : IN Boolean) is
	begin
		if Instruction then
			case Current is
				when Direction'Last => Current := Direction'First;
				when others => Current := Direction'Succ(Current);
			end case;
		else
			case Current is
				when Direction'First => Current := Direction'Last;
				when others => Current := Direction'Pred(Current);
			end case;
		end if;
	end UpdateDirection;
	-- Makes the robot go toward current direction
	procedure StepForward(CurrentDirection : IN Direction ; CurrentPosition : IN OUT BoardPositionPtr) is
		NewPosition : BoardPositionPtr := new BoardPosition'(CurrentPosition.X, CurrentPosition.Y, False, CurrentPosition);
		ExplorePosition : BoardPositionPtr := CurrentPosition;
	begin
		loop
			if (ExplorePosition.X = NewPosition.X and ExplorePosition.Y = NewPosition.Y) then
				NewPosition.State := ExplorePosition.State;
				exit;
			end if;
			ExplorePosition := ExplorePosition.Previous;
			exit when ExplorePosition = null;
		end loop;
		CurrentPosition := NewPosition;
	end StepForward;
	procedure RunRobot(minX, minY, maxX, maxY : OUT Integer ; LastPosition : OUT BoardPositionPtr) is
		CurrentDirection : Direction := Up;
	begin
		minX := 0;
		minY := 0;
		maxX := 0;
		maxY := 0;
		LastPosition := new BoardPosition'(0, 0, False, null);
		--loop
		--run intcode with LastPosition.Status as input
		--retrieve output, update LastPosition.Status and CurrentDirection
		--use UpdateDirection
		--use StepForward
		--end loop when finished
	end RunRobot;
begin
	null;
end PaintingRobot;

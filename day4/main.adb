with ValidPwd;
with Ada.Text_IO; use Ada.Text_IO;

procedure Main is
	RangeMin : Integer := 278384;
	RangeMax : Integer := 824795;
	Count : Integer := 0;
begin
	for Password in RangeMin..RangeMax loop
		if ValidPwd.IsValid(Password) then
			Count := Count+1;
		end if;
	end loop;
	Put_Line("Number of passwords :" & Count'Image);
end Main;

